﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("test.csv");
            User FirstUser = User.GenerateUser("Adrian");
            ProtectionProxyDataset first = new ProtectionProxyDataset(FirstUser);
            VirtualProxyDataset second = new VirtualProxyDataset("test.csv");
            DataConsolePrinter canonPrinter = new DataConsolePrinter();
            canonPrinter.print(first);
            canonPrinter.print(second);

            User SecondUser = User.GenerateUser("Test");
            ProtectionProxyDataset test= new ProtectionProxyDataset(SecondUser);
            canonPrinter.print(test);

            ITheme ThemeTest1 = new LightTheme();
            ReminderNote note1 = new ReminderNote("Test1", ThemeTest1);
            note1.Show();

            ITheme ThemeTest2 = new YellowTheme();
            ReminderNote note2 = new ReminderNote("Test2", ThemeTest2);
            note2.Show();

        }
    }
}
