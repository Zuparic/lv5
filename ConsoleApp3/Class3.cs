﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class ShippingService
    {
        private double massprice;
        public ShippingService (double MassPrice)
        {
            massprice = MassPrice;
        }


        public double priceship(IShipable Mass)
        {
            double mass = Mass.Weight ;
            return mass * massprice;
        }
    }
}
